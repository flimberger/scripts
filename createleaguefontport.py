#!/usr/bin/env python3
# Copyright (c) 2019 Florian Limberger <flo@snakeoilproductions.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import sys

from io import BytesIO
from urllib.request import urlopen
from zipfile import ZipFile


PORTS_DIR = '/usr/ports'
CATEGORY = 'x11-fonts'
MAINTAINER_NAME = 'Florian Limberger'
MAINTAINER_EMAIL = 'flo@snakeoilproductions.net'

README_FILE = 'readme.markdown'
LICENSE_FILE = 'Open Font License.markdown'

class Port:
    pass


def write_makefile(info):
    # filename = PORTS_DIR + '/' + CATEGORY + '/' + info.name + '/Makefile'
    filename = 'Makefile'
    with open(filename, 'w') as f:
        f.write(f"""# Created by: {MAINTAINER_NAME} <{MAINTAINER_EMAIL}>
# $FreeBSD$

PORTNAME=	{info.name}
DISTVERSION=	{info.distversion}
CATEGORIES=	{CATEGORY}

MAINTAINER=	{MAINTAINER_EMAIL}
COMMENT=	{info.comment}

LICENSE=	{info.license}
LICENSE_FILE=	${{WRKSRC}}/Open\\ Font\\ License.markdown

USES=		fonts
USE_GITHUB=	yes
GH_ACCOUNT=	theleagueof
GH_TAGNAME=	{info.tagname}

NO_ARCH=	yes
NO_BUILD=	yes

PLIST_FILES=	${{FONTSDIR}}/{info.files}

do-install:
	${{MKDIR}} ${{STAGEDIR}}${{FONTSDIR}}
	${{INSTALL_DATA}} ${{WRKSRC}}/{info.files} ${{STAGEDIR}}${{FONTSDIR}}

.include <bsd.port.mk>
""")

def usage():
    print(f'usage: {sys.argv[0]} github-URL', file=sys.stderr)
    sys.exit(1)


def main(args):
    if len(args) < 2:
        usage() 

    github_name = args[1]
    otf_files = []
    name = ''
    with urlopen(f'https://github.com/theleagueof/{github_name}/archive/'
                 'master.zip') as f:
        with ZipFile(BytesIO(f.read()), 'r') as zf:
            for entry in zf.namelist():
                if entry.endswith('.otf'):
                    otf_files.append(entry)
            with zf.open(README_FILE) as readme:
                for line in readme:
                    if line and not line.startswith('!'):
                        name = line
                        break
        print(otf_files)
    info = Port()
    info.name = name
    info.distversion = '123'
    info.comment = 'A geometric sans-serif from the future'
    info.license = 'OFL'
    info.tagname = '456'
    info.files = otf_files
    write_makefile(info)
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
