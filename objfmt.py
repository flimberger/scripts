#!/usr/bin/env python3
# Copyright (c) 2020 Florian Limberger <flo@purplekraken.com>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""
Print the type of object files

This utility checks the magic number at the beginning of its input files
whether it matches any of its know object formats. Currently, only ELF
binaries, LLVM IR files and PE binaries are supported.
"""

import sys


def search_files(filenames):
    for fn in filenames:
        with open(fn, 'rb') as f:
            magic = f.read(4)
            # This assumes LE, like x86 or amd64!
            fmtn = 'unknown'
            if magic == b'BC\xc0\xde':
                fmtn = 'LLVM IR'
            elif magic == b'\x7fELF':
                fmtn = 'ELF'
            # MZ is the DOS header, which is present on backwards compatible
            # Windows binaries. The actual magic number is 'PE\x00\x00'.
            elif magic[:2] == b'MZ' or magic == b'PE\x00\x00':
                fmtn = 'PE'
            if fmtn != 'unknown':
                print(f'{fn}: {fmtn}')


def main():
    search_files(sys.argv[1:])


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
