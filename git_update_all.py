#!/usr/bin/env python3
# Copyright (c) 2019 Florian Limberger <flo@snakeoilproductions.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""Visit all directories and if it is a git repo, run ``git pull
--ff-only``."""

import subprocess
import sys

from enum import Enum
from pathlib import Path
from typing import List, NamedTuple


PROGNAME = 'git_update_all'


EXCLUDE_DIRS = [
    '__pycache__',
    'build',  # often used in Google projects
    'prebuilts',  # often used in Google projects
    'obj',
    'regress',  # unit tests in OpenBSD code
]


class Verbosity(Enum):
    QUIET = 0
    NORMAL = 1
    VERBOSE = 2


verbosity = Verbosity.NORMAL


# TODO: a table mapping scm enum to their repo dir names would be nice
class Scm(Enum):
    GIT = 0
    MERCURIAL = 1
    SUBVERSION = 2
    REPO = 3


class UpdateStatus(NamedTuple):
    path: str
    returncode: int


class RepoException(Exception):
    def __init__(self, path: Path, msg: str):
        super().__init__(msg)
        self.path = path


def fatal(msg: str):
    print(f'{PROGNAME}: {msg}', file=sys.stderr)
    sys.exit(1)


def get_subdirs(path: Path) -> List[Path]:
    return [subdir for subdir in path.iterdir() if subdir.is_dir()]


def get_repository_types(subdirs: List[Path]) -> List[Scm]:
    """Get a list of SCMs which have repositories in the current directory,
    or empty if there is none."""

    scms: List[Scm] = []
    for subdir in subdirs:
        if subdir.name == ".git":
            scms.append(Scm.GIT)
        elif subdir.name == ".hg":
            scms.append(Scm.MERCURIAL)
        elif subdir.name == ".svn":
            scms.append(Scm.SUBVERSION)
        elif subdir.name == ".repo":
            scms.append(Scm.REPO)
    return scms


def update_path(path: Path) -> List[UpdateStatus]:
    update_status: List[UpdateStatus] = []
    subdirs = get_subdirs(path)
    scms = get_repository_types(subdirs)
    if len(scms) == 0:
        if verbosity == Verbosity.VERBOSE:
            print(f'descending to "{str(path)}"')
        for subdir in subdirs:
            if subdir.name[0] != '.' and subdir.name not in EXCLUDE_DIRS:
                update_status += update_path(subdir)
    elif len(scms) > 1:
        raise RepoException(path, f'"{str(path)}" contains multiple '
                                  'repositories, which is currently not '
                                  'supported')
    elif len(scms) == 1 and scms[0] == Scm.GIT:
        print(str(path) + ':')
        cmdline = ['git', '-C', str(path), 'pull', '--ff-only']
        if verbosity == Verbosity.QUIET:
            cmdline.append('-q')
        elif verbosity == Verbosity.VERBOSE:
            cmdline.append('-v')
        completed_process = subprocess.run(cmdline)
        update_status.append(UpdateStatus(path.as_posix(),
                                          completed_process.returncode))
    else:
        if verbosity == Verbosity.VERBOSE:
            print(f'"{str(path)}" is managed by another SCM')

    return update_status


def main(args: List[str]) -> int:
    if not args:
        args = ['.']
    global verbosity
    if args[0] == '-v':
        verbosity = Verbosity.VERBOSE
        args = args[1:]
    elif args[0] == '-q':
        verbosity = Verbosity.QUIET
        args = args[1:]
    update_status: List[UpdateStatus] = []
    retval = 0
    errors: List[RepoException] = []
    for arg in args:
        path = Path(arg)
        if path.exists():
            try:
                update_status += update_path(path)
            except RepoException as ex:
                errors.append(ex)
        else:
            print(f'invalid path: {path}', file=sys.stderr)
            retval = 1
    if update_status and verbosity != Verbosity.QUIET:
        print('\nupdated repositories:')
        for s in update_status:
            status_str = 'SUCCESS' if s.returncode == 0 \
                                   else f'FAILED({s.returncode})'
            print(f'\t{s.path}: {status_str}')
    # TODO: this might not be optimal
    if len(errors) > 0:
        for err in errors:
            print(f'{PROGNAME}: {err}', file=sys.stderr)
    return retval


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
