#!/bin/sh
# Copyright (c) 2019 Florian Limberger <flo@snakeoilproductions.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Install a Java distribution into the debian alternatives database and set
# the default.

set -eu

PATH="/bin:/sbin:/usr/bin:/usr/sbin"

usage() {
	echo "usage: $0 -i|-s javahome priority" >&2
	exit 2
}

install_alternatives() {
	for prog in $java_progs; do
		update-alternatives --install	\
		    "$bindir/$prog" "$prog" "$java_bindir/$prog" "$priority"
	done
}

set_alternatives() {
	for prog in $java_progs; do
		update-alternatives --set "$prog" "$java_bindir/$prog"
	done
}

if [ $# -lt 3 ]; then
	usage
fi

operation="$1"
java_home="$2"
# TODO: parse the priority from the version
priority="$3"

if [ ! -d "$java_home" ]; then
	echo "fatal: $2 is not a directory" >&2
	usage
fi

if [ ! -x "$java_home/bin/java" ]; then
	echo "fatal: $2 doesn't contain a java executable" >&2
	usage
fi

bindir=/usr/bin
# This is not POSIX, but the alternatives system is Debian anyway
java_bindir=$(realpath "$java_home/bin")
java_progs="appletviewer 
	extcheck \
	idlj \
	jar \
	jarsigner \
	java \
	javac \
	javadoc \
	javah \
	javap \
	java-rmi.cgi \
	jcmd \
	jconsole \
	jdb \
	jdeps \
	jhat \
	jinfo \
	jjs \
	jmap \
	jps \
	jrunscript \
	jsadebugd \
	jstack \
	jstatd \
	keytool \
	native2ascii \
	orbd \
	pack200 \
	policytool \
	rmic \
	rmid \
	rmiregistry \
	schemagen \
	serialver \
	servertool \
	tnameserv \
	unpack200 \
	wsgen \
	wsimport \
	xjc \
	"

case "$operation" in
-i)	install_alternatives ;;
-s)	set_alternatives ;;
*)	usage ;;
esac
