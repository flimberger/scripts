#!/bin/sh
set -eu

j=r120-amd64
p=devel

if [ $# -lt 1 ]
then
	echo "usage: $0 PORT" >&2
	exit 1
fi

sudo poudriere testport\
	-j "$j"\
	-p "$p"\
	-o "$@"
