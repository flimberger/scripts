DESTDIR?=	${HOME}
BINDIR?=	/bin/scripts

INSTALL =	install

all:
	@echo 'Nothing to build, use `make install` to install the scripts' >&2;\
	exit 1
.PHONY: all

install:
	${INSTALL} -C G ${DESTDIR}${BINDIR}/G
	${INSTALL} -C git_update_all.py ${DESTDIR}${BINDIR}/git_update_all
	${INSTALL} -C E ${DESTDIR}${BINDIR}/E
	${INSTALL} -C copysel ${DESTDIR}${BINDIR}/copysel
	${INSTALL} -C c++-new-class.py ${DESTDIR}${BINDIR}/c++-new-class
	${INSTALL} -C objfmt.py ${DESTDIR}${BINDIR}/objfmt
	${INSTALL} -C pip_update ${DESTDIR}${BINDIR}/pip_update
	${INSTALL} -C skip ${DESTDIR}${BINDIR}/skip
	${INSTALL} -C touchpadctl ${DESTDIR}${BINDIR}/touchpadctl
	${MAKE} install-$$(uname)
.PHONY: install

install-FreeBSD:
	${INSTALL} -C createjail.sh ${DESTDIR}${BINDIR}/createjail.sh
	${INSTALL} -C jailpatch.sh ${DESTDIR}${BINDIR}/jailpatch.sh
	${INSTALL} -C updatepoudriere.sh ${DESTDIR}${BINDIR}/updatepoudriere.sh
	${INSTALL} -C testport.sh ${DESTDIR}${BINDIR}/testport.sh
.PHONY: install-FreeBSD

install-Linux:
	${INSTALL} -C s3-cp ${DESTDIR}${BINDIR}/s3-cp
	${INSTALL} -C dpkg-config-purge ${DESTDIR}${BINDIR}/dpkg-config-purge
	${INSTALL} -C ubuntu-upgrade ${DESTDIR}${BINDIR}/ubuntu-upgrade
	${INSTALL} -C ubuntu-version ${DESTDIR}${BINDIR}/ubuntu-version
	${INSTALL} -C update-clang-alternatives ${DESTDIR}${BINDIR}/update-clang-alternatives
	${INSTALL} -C update-java-alternatives ${DESTDIR}${BINDIR}/update-java-alternatives
.PHONY: install-Linux
