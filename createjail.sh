#!/bin/sh
# Copyright (c) 2019 Florian Limberger <flo@snakeoilproductions.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Create and initialize a new jail.

set -eu

# TODO: add new IP to aliases in rc.conf
# TODO: add new IP to pf.conf

usage() {
	echo "usage: $0 name ip4addr" >&2
	exit 1
}

if [ $# -lt 3 ]; then
	usage
fi

name="$1"
ip4addr="$2"

base=12.0-RELEASE
domain=purplekraken.com
iocroot=/zroot/iocage

sudo iocage create \
    -r "$base" -b \
    -n "$name" \
    host_hostuuid="$name" \
    host_hostname="$name.$domain" \
    ip4_addr="lo1|$ip4addr"

# resolv.conf and timezone are copied by iocage
sudo cp -r /usr/local/etc/pkg "$iocroot/jails/$name/root/usr/local/etc"
