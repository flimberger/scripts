#!/usr/bin/env python3
# Copyright (c) 2020 Florian Limberger <flo@purplekraken.com>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""
Generate C++ class source code

``C++-new-class`` is a utility to generate the source files for C++
classes.  It uses separate directories for the source code and include
files, named ``src`` and ``include`` respectively. If a path is
specified, the subdirectories are created below the parent directories.
Also, it is interpreted as a hierarchy of namespaces, below which the
class defined.
"""

import sys
import os
from enum import Enum


HDR_DIR = 'include'
HDR_EXT = 'hpp'
SRC_DIR = 'src'
SRC_EXT = 'cpp'


class CppClassInfo:
    def __init__(self, name, path, namespace):
        self.name = name
        self.path = path
        self.namespace = namespace

    def include_guard(self):
        '''Return the name of the include guard macro symbol.'''

        elems = self.path.split(os.sep)
        # TODO: CamelCase names need more processing
        elems += [self.name, HDR_EXT]
        return '_'.join(elems).upper()

    def include_file(self):
        '''Return the path of the include file, suitable for #include.'''

        elems = self.path.split(os.sep)
        elems.append(f'{self.name}.{HDR_EXT}')
        return '/'.join(elems)


def write_namespace(f, namespace, closing=False):
    if len(namespace) > 0:
        if closing:
            f.write('\n')
            for n in reversed(namespace):
                f.write(f'}}  // namespace {n}\n')
        else:
            for n in namespace:
                f.write(f'namespace {n} {{\n')
            f.write('\n')


def write_header(f, class_info):
    include_guard = class_info.include_guard()
    name = class_info.name
    namespace = class_info.namespace
    f.write(f'''#ifndef {include_guard}
#define {include_guard}

''')
    write_namespace(f, namespace)
    f.write(f'''class {name} {{
public:
    {name}();
}};
''')
    write_namespace(f, namespace, closing=True)
    f.write(f'\n#endif  // {include_guard}\n')


def write_source(f, class_info):
    name = class_info.name
    namespace = class_info.namespace
    f.write(f'#include "{class_info.include_file()}"\n\n')
    write_namespace(f, namespace)
    f.write(f'{class_info.name}::{class_info.name}() = default;\n')
    write_namespace(f, namespace, closing=True)


class FileType(Enum):
    def __init__(self, basedir, ext, writefunc):
        self.basedir = basedir
        self.ext = ext
        self.write_impl = writefunc

    HEADER = (HDR_DIR, HDR_EXT, write_header)
    SOURCE = (SRC_DIR, SRC_EXT, write_source)

    def write(self, class_info):
        srcdir = os.path.join(self.basedir, class_info.path)
        srcfile = os.path.join(srcdir, f'{class_info.name}.{self.ext}')
        os.makedirs(srcdir, exist_ok=True)
        with open(srcfile, 'w') as f:
            self.write_impl(f, class_info)


def main():
    if len(sys.argv) < 2:
        print('usage: c++-new-class name [path]', file=sys.stderr)
        sys.exit(1)
    name = sys.argv[1]
    path = ''
    try:
        path = sys.argv[2].strip(os.sep)
    except IndexError:
        pass
    namespace = [] if path == '' else path.split(os.sep)
    class_info = CppClassInfo(name, path, namespace)
    for t in FileType:
        t.write(class_info)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit(1)
